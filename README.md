# Open Documentation

This is the documentation of a properitary game.

## Read

Don't try to read the documentation on Gitlab. It's a mess. You can read the entire documentation here: https://open-documentation.readthedocs.io/en/latest/

## Contribute

Feel free to contribute any information you have got. You can either make a merge request or just create an issue regarding your topic.

## Translate

Contribution language is english, but feel free to translate existing articles to foreign languages. Please don't contribute new articles in foreign languages.
