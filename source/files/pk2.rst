======================
Joymax Pak File (.pk2)
======================

Abstract
========

Joymax Pak File is used to store the games data files. The format is fragmented; index data and content data are stored in no specific location. Index data is stored in blocks of X entries and is encrypted using Blowfish (LINK). The content data is neither encrypted nor compressed in any way.

Format
======

Magic
-----

Header
------

Index data
----------

Data
----

Nothing special. Data is stored without protection or encryption.

Trivia
======

* Joymax's implementation does not support defragmentation. Pak Files can only grow.
* Joymax's implementation uses a "plain text password" instead of the plain blowfish key. The blowfish key is generated from the password.
* 
