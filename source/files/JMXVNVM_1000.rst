============
JMXVNVM 1000
============

Relevant for:

+---------+-----------+
| Region  | Version   |
+=========+===========+
| Unknown | Open Beta |
+---------+-----------+
