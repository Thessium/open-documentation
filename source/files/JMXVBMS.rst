===========
Mesh (.bms)
===========

Stores one (partial) mesh of a resource. Due to vertice count limitations, a mesh may be split into multiple separated mesh-files.


.. toctree::
    :maxdepth: 1

    JMXVBMS_0110
..
