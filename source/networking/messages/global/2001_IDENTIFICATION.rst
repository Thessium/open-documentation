=====================
IDENTIFICATION (2001)
=====================

Used for telling the remote end the identity/purpose of our local end. The endpoint will also use this to verify that the other endpoints identity is compatible with its own. e.g. the GatewayServer will only talk to SR_Client's on it's public port.

Invalid identities will lead to an immediate termination of the connection.

+-----------+---------+----------+-------------------------------------------------------------+
| Size      | Type    | Name     | Desc                                                        |
+===========+=========+==========+=============================================================+
| ``*``     | string  | Identity | Type of the module                                          |
+-----------+---------+----------+-------------------------------------------------------------+
| ``1``     | bool    | IsLocal  | ``false`` = Machine to Machine, ``true`` = Module to Module |
+-----------+---------+----------+-------------------------------------------------------------+

Valid identities are:

* SR_Client
* GatewayServer
* AgentServer
* etc.
