Count/Security Byte
===================

Each message is stamped with a counter. This is done to avoid replay-attacks and message injection. Basically each message gets an increasing number. All messages need to stay in order. If a number appears out of order, the connection will be terminated.

To make it less obvious, the game uses a pseudo-random-number-generator algorithm. It's seeded with an initial value and will generate unobvious numbers. Security by obscurity was already a thing back then.

Sadly, the name of the used algorithm is still unknown. It's operations have been reverse engineered, but its still a big chunk of unreadable code.

TODO: Put code here

