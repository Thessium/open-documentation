Files
=====

Over the years, many of the properitary file formats have been reverse engineered. This is a collection of known information.

2D Graphics
-----------

.. toctree::
    :maxdepth: 1

    files/2dt
    files/dat_bitmap
    files/ddj
    files/txt_window
..

3D Graphics
-----------

.. toctree::
    :maxdepth: 1

    files/cpd
    files/JMXVBAN
    files/JMXVBMS
    files/JMXVBMT
    files/JMXVBSK
    files/JMXVRES
    files/efp
..

Map / Environment
-----------------

.. toctree::
    :maxdepth: 1

    files/JMXVNVM
    files/dat_ainavdata

    files/JMXVDOF

    files/JMXVOBJI

    files/JMXVMAPM
    files/JMXVMAPO
    files/t
..


Shader
------

.. toctree::
    :maxdepth: 1

    files/psh
    files/vsh
..

Audio
-----

.. toctree::
    :maxdepth: 1

    files/ogg
..


Container
---------

.. toctree::
    :maxdepth: 1

    files/pk2
..




* https://www.elitepvpers.com/forum/sro-coding-corner/1992824-wip-silkroad-file-formats-bsr-bms-bmt-bsk-ban.html
* https://www.elitepvpers.com/forum/sro-coding-corner/3854560-release-filestructure-jmxvdof-0101-a.html
